var json = {"docType":"html","elements":[{"nodeType":1,"tag":"HTML","attributes":[],"children":[{"nodeType":1,"tag":"HEAD","attributes":[],"children":[{"nodeType":3,"textContent":"\n    "}]},{"nodeType":3,"textContent":"\n    "},{"nodeType":1,"tag":"BODY","attributes":[],"children":[{"nodeType":3,"textContent":"\n        "},{"nodeType":1,"tag":"DIV","attributes":[],"children":[{"nodeType":3,"textContent":"Hi"}]},{"nodeType":3,"textContent":"\n        "},{"nodeType":1,"tag":"SCRIPT","attributes":[{"name":"type","value":"text/javascript"},{"name":"src","value":"domtojson.js"}],"textContent":null}]}]}]}

var iframeDoc = document.getElementById('iframe').contentWindow.document;
iframeDoc.removeChild(iframeDoc.getElementsByTagName('html')[0]);
var ifId = 0;
createDom(json.elements[0], iframeDoc, false);
function createDom(obj, doc, resetId) {
    if(obj.nodeType !== 3) {
        ifId++;
        var el = document.createElement(obj.tag);
        el.setAttribute("id", "if-"+ifId+"");
        if(obj.attributes !== undefined) {
            for (var i=0;i<obj.attributes.length;i++) {
                var attribute = obj.attributes[i];
                if(attribute.name !== 'id') {
                    el.setAttribute(attribute.name, attribute.value);
                }
            }
        }
        idRef = "if-"+ifId+"";
        doc.appendChild(el);
    }
    else {
        doc.appendChild(iframeDoc.createTextNode(obj.textContent));
    }
    console.log(obj, resetId);
    if(resetId) {
        idRef = iframeDoc.getElementById(idRef).parentElement.getAttribute("id");
    }
    if(obj.children !== undefined) {
        var children = obj.children;
        var reset = false;
        for (k=0;k<children.length;k++) {
            if((k === children.length-1) && (children[k].nodeType === 3)) {
                reset = true;
            }
            createDom(children[k], iframeDoc.getElementById(idRef), reset);
        }
    }
}
