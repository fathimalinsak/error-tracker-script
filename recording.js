var visitData = {};
var ipDetails = {};
var idleTime = 60000;

function startSession() {
	setUserId();
	setSessionId();
	var userAgent = navigator.userAgent;
	visitData.user_agent = userAgent;
	visitData.start_time = new Date();
	visitData.end_time = new Date();
	console.log(typeof visitData.start_time);
	setCookie("recsession", true, 365);
	getIpDetails();
	setTimeout(function(){ 
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "http://localhost/error-tracker/public/api/v1/record", true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(visitData));
	}, 3000);
}

function getBrowserDetails() {
	var ua = navigator.userAgent, tem,
		M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	if (/trident/i.test(M[1])) {
		tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
		return { name: 'IE', version: (tem[1] || '') };
	}
	if (M[1] === 'Chrome') {
		tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
		if (tem != null) return { name: tem[1].replace('OPR', 'Opera'), version: tem[2] };
	}
	M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
	if ((tem = ua.match(/version\/(\d+)/i)) != null)
		M.splice(1, 1, tem[1]);
	return { name: M[0], version: M[1] };
}

function getIpDetails() {
	var request = new XMLHttpRequest();
	request.open('GET', 'https://ipapi.co/json/', true);

	request.onload = function () {
		if (this.status >= 200 && this.status < 400) {
			// Success!
			var data = JSON.parse(this.response);
			getIpSuccessCallBack(data);
		} else {
			// We reached our target server, but it returned an error
		}
	};
	request.onerror = function () {
		// There was a connection error of some sort
	};

	request.send();
}

function getIpSuccessCallBack(data) {
	ipDetails = data;
	visitData.location_data = ipDetails;
}

function inactivityTime() {
	var time;
	window.onload = resetTimer;
	var events = ['mousedown', 'mousemove', 'keypress', 'scroll', 'touchstart'];
	events.forEach(function (name) {
		document.addEventListener(name, resetTimer, true);
	});

	function resetTimer() {
		clearTimeout(time);
		time = setTimeout(endSession, idleTime)
	}
};

function endSession() {
	setCookie("recsession", false, 365);
	setCookie("sessionId", '', 365);
	console.log("session ended");
}

function checkSession() {
	console.log(getCookie("recsession"));
	if((getCookie("recsession") != '') && (JSON.parse(getCookie("recsession")))) {
		console.log("continue recording in same session", getCookie('sessionId'));
	}
	else {
		console.log("starting a new session");
		startSession();
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function setUserId() {
	if(sessionStorage.getItem("erId") != undefined) {
		visitData.visitor_id = sessionStorage.getItem("erId");
	}
	else {
		var userId = randomString(6);
		sessionStorage.setItem("erId", userId);
		visitData.visitor_id = userId;
	}
}

function setSessionId() {
	if(getCookie("sessionId") != '') {
		visitData.session_id = getCookie("sessionId");
	}
	else {
		var sessionId = randomString(6);
		setCookie("sessionId", sessionId);
		visitData.session_id = sessionId;
	}
}

function randomString(length) {
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

function websocket() {
	let socket = new WebSocket("ws://localhost:6001/app/websocketkey?protocol=7&client=js&version=4.3.1&flash=false");
	var data = {
		"event": "message",
		"data" : "test"
	}
	socket.onopen = function(e) {
		alert("[open] Connection established");
		alert("Sending to server");
		socket.send(JSON.stringify(data));
	};

	socket.onmessage = function(event) {
		alert(`[message] Data received from server: ${event.data}`);
	};

	socket.onclose = function(event) {
		if (event.wasClean) {
			alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
		} else {
			// e.g. server process killed or network down
			// event.code is usually 1006 in this case
			alert('[close] Connection died');
		}
	};

	socket.onerror = function(error) {
		alert(`[error] ${error.message}`);
	};
}

function getContent() {
	var domJson = {};

    domJson.docType = document.doctype!=undefined? document.doctype.name: '';
    var domElements = {};
    domElements = getDomJson(document.getElementsByTagName('html')[0], {});
    domJson.elements = [];
    domJson.elements[0] = domElements;

    console.log(JSON.stringify(domJson));
}

function getDomJson(obj, json) {
    json.nodeType = obj.nodeType;
    if(obj.nodeType !== 3) {
        json.tag = obj.tagName;
        json.attributes = [];
        for(var k=0;k<obj.attributes.length;k++) {
            json.attributes[k] = {};
            json.attributes[k].name = obj.attributes[k].name;
            json.attributes[k].value = obj.attributes[k].value;
        }
    }
    var childrenCount = obj.childNodes.length;
    if(childrenCount) {
        json.children = [];
        var children = obj.childNodes;
        for (var i=0;i<childrenCount;i++) {
            json.children[i] = {};
            domElements = getDomJson(children[i], json.children[i]);
        }
    }
    else {
        json.textContent = obj.nodeValue;
    }
    return json;
}

window.onload = function () {
	inactivityTime();
	checkSession();
	getContent();
	// websocket();
}