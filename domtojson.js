

function getDomJson(obj, json) {
    json.nodeType = obj.nodeType;
    if(obj.nodeType !== 3) {
        json.tag = obj.tagName;
        json.attributes = [];
        for(var k=0;k<obj.attributes.length;k++) {
            json.attributes[k] = {};
            json.attributes[k].name = obj.attributes[k].name;
            json.attributes[k].value = obj.attributes[k].value;
        }
    }
    var childrenCount = obj.childNodes.length;
    if(childrenCount) {
        json.children = [];
        var children = obj.childNodes;
        for (var i=0;i<childrenCount;i++) {
            json.children[i] = {};
            domElements = getDomJson(children[i], json.children[i]);
        }
    }
    else {
        json.textContent = obj.nodeValue;
    }
    return json;
}

function getContent() {
    var domJson = {};

    domJson.docType = document.doctype!=undefined? document.doctype.name: '';
    var domElements = {};
    domElements = getDomJson(document.getElementsByTagName('html')[0], {});
    domJson.elements = [];
    domJson.elements[0] = domElements;

    console.log(JSON.stringify(domJson));
}

getContent();